BukkitPluginUtil is a collection with important classes and libraries which I need to develop. 
BukkitPluginUtil includes:

- [CommandHandler](https://github.com/PneumatiCraft/CommandHandler) by [PneumatiCraft](http://pneumaticraft.com)
- A Language class (which makes easy to create multilang porjects).
- A Permission Interface (PermissionInterface.java).
- A class with important functions like Pythons [str.join()](http://docs.python.org/2/library/stdtypes.html#str.join) function 

### License ###
BukkitPluginUtil is realesed under the BSD license.

