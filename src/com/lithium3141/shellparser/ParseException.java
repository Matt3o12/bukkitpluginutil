package com.lithium3141.shellparser;

public class ParseException extends Exception {
	private static final long serialVersionUID = 7408551094292750107L;

	public ParseException() {
        super();
    }
    
    public ParseException(String string) {
        super(string);
    }

}
