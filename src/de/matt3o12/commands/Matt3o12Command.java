package de.matt3o12.commands;

import static de.matt3o12.utils.Language._;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import com.pneumaticraft.commandhandler.Command;

/**
 * It add lots of methods and essentials functions to pneumaticrafts {@link Command}.<br>
 * Every command should include it.
 * 
 * @author Matt3o12
 */
abstract public class Matt3o12Command extends Command implements SimpleCommand {
	/** An array with all aliases. */
	protected static HashMap<String, ArrayList<String>> aliases;

	/** Command description. */
	protected String description;
	
	/** Deprecated. */
	private boolean displayInHelpPage = true;
	
	/** The {@link CommandType}. (Is the command a user, op, or developer command. */
	private CommandType commandType;
	
	/**
	 * Set up lots of essentials methos. You have to call it when you override the constructor.
	 * 
	 * @param plugin Your main plugin.
	 */
	
	static{
		aliases = new HashMap<String, ArrayList<String>>();
	}
	
	public Matt3o12Command(Plugin plugin) {
		super(plugin);
		setCommandType(CommandType.USER_COMMAND);
	}

	@Override
	public abstract void runCommand( CommandSender sender, List<String> args );
	
	/**
	 * It is inchoate!
	 * 
	 * @param sender Unimportant.
	 * @return Always true.
	 */
	public boolean isDeveloper( CommandSender sender ){
		return true;
	}
	
	/**
	 * It return if the command should be displayed in the help page
	 * 
	 * @return True if it should be displayed.
	 * @deprecated
	 */
	@Deprecated
	public boolean diplayInHelpPage(){
		return displayInHelpPage;
	}
	
	/**
	 * Set if the command should be display in the help page.
	 * 
	 * @param displayInHelpPage True if it should be displayed.
	 * @deprecated
	 */
	@Deprecated
	public void setDisplayInHelpPage( boolean displayInHelpPage ){
		this.displayInHelpPage = displayInHelpPage;
	}
	
	/**
	 * Set the current command description.<br>
	 * It should be called by your construct.
	 * 
	 * @param description The description.
	 */
	public void setCommandDesc( String description ){
		this.description = description;
	}
	
	@Override
	/**
	 * Return the command description.
	 */
	public String getCommandDesc(){
		return description;
	}

	/**
	 * Return the help colour.
	 * 
	 * @see CommandType#getChatColor()
	 * @deprecated Use {@link CommandType#getChatColor()}
	 */
	@Deprecated
	public ChatColor getHelpColor(){
		return commandType.getDefuleChatColor();
	}
	
	/**
	 * Set the help colour.
	 * 
	 * @param chatColor The new help colour.
	 * @deprecated You should use: {@link Matt3o12Command#setCommandType(CommandType)}.
	 */
	@Deprecated
	public void setHelpColor( ChatColor chatColor ){
		switch ( chatColor ){
		case YELLOW:
			setCommandType( CommandType.OPERATOR_COMMAND );
			break;
			
		case RED:
			setCommandType( CommandType.DEVELOPER_COMMAND );
			break;
		
		case AQUA:
		default:
			setCommandType( CommandType.USER_COMMAND );
			break;
		}
	}
	
	/**
	 * Set the {@link CommandType}
	 * 
	 * @param commandType The new {@link CommandType}
	 */
	public void setCommandType( CommandType commandType ){
		this.commandType = commandType;
	}
	
	@Override
	public boolean equals( Object o ){
		if (! ( o instanceof SimpleCommand ) )
			return super.equals(o);
		
		
		SimpleCommand a = ( SimpleCommand ) o;
		if ( this.getCommandName() == a.getCommandName() )
			return true;
		
		return super.equals(o);
	}
	
	@Override
	public int compareTo( SimpleCommand command ){
		final int BEFOR = -1;
		final int EQUALS = 0;
		final int AFTER = 1;
		
		if ( command.equals( this ) )
			return EQUALS;
				
		if ( command.getCommandType().getPosition() > getCommandType().getPosition() )
			return BEFOR;
		
		return AFTER;
	}
	
	/**
	 * It returns the command type.
	 */
	@Override
	public CommandType getCommandType(){
		return commandType;
	}
	
	/**
	 * Author see: {@link Command#showHelp(CommandSender)}, (changes by Matt3o12).
	 * Changes:
	 * - Improve {@link ChatColor}s.
	 */
	@Override
	public void showHelp(CommandSender sender) {
		String m1 = _("commandHelp.desc", "Description:");
		String m2 = _("commandHelp.usage", "Usage:");
		
        sender.sendMessage(ChatColor.AQUA + "------- " + ChatColor.DARK_AQUA + this.getCommandName() + ChatColor.AQUA + " -------");
        sender.sendMessage(" " + ChatColor.AQUA + m1 + ChatColor.DARK_AQUA + " " + this.getCommandDesc());
        sender.sendMessage(" " + ChatColor.AQUA + m2 + ChatColor.DARK_AQUA + " " + this.getCommandUsage());
        
        if (this.getCommandExamples().size() > 0) {
            sender.sendMessage(ChatColor.GOLD + "" + _("commandHelp.examples", "Examples: "));
            if (sender instanceof Player) {
                for (int i = 0; i < 4 && i < this.getCommandExamples().size(); i++) {
                    sender.sendMessage(" - " + this.getCommandExamples().get(i));
                }
            } else {
                for (String c : this.getCommandExamples()) {
                    sender.sendMessage("   " + ChatColor.AQUA + c);
                }
            }
        }
    }
	
	@Override
	public void addKey(String key) {
		String[] keySplited = key.split(" ", 2);
		String command = keySplited[0].trim();
		String others;
		if (keySplited.length > 1)
			others = keySplited[1].trim();
		else
			others = "";
		
		List<String> aliases = getAliases(command);
		if (aliases != null) {
			for (String alias : aliases) {
				addKey(alias + " " + others);
			}
		}
		
		super.addKey(key);
	}
	
	/**
	 * Add a command alias.<br>
	 * For example: If your main command is "/timebutton" 
	 * and you want that your plugin is able to handle "/tb" command, too
	 * you must only add the alias.<br>
	 * Works with subcommands ("/timebutton add" is also equals with "/tb add"
	 * if you add "tb" as alias for "timebutton").
	 * 
	 * @param mainCommand The main command (See example).
	 * @param alias The alias.
	 */
	protected static void addAlias(String mainCommand, String alias){
		ArrayList<String> theAliases = aliases.get(mainCommand);
		if (theAliases == null)
			theAliases = new ArrayList<String>();
		
		theAliases.add(alias);
		aliases.put(mainCommand, theAliases);
	}
	
	/**
	 * Returns all aliases for a command.<br>
	 * <br>
	 * Usually, you needn't call the method.
	 * @param command Equals with mainCommand by the addAlias method.
	 * @return A {@link List} with all aliases.
	 */
	public static List<String> getAliases(String command){
		return aliases.get(command);
	}
}
