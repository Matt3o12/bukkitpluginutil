package de.matt3o12.commands;

import org.bukkit.permissions.Permission;

/**
 * Every command should include it. It doesn't matter whether if the command use Pneumaticrafts CommandHandler or not.<br>
 * If you are going to include your command to a {@link AbstractHelpPage} HelpPage you will need only include the interface and not 
 * {@link Matt3o12Command}.<br><br>
 * If you are using Pneumaticrafts CommandHanlder (and you are including my expansion {@link Matt3o12Command}) you will not need
 * the interface because {@link Matt3o12Command} include it already.
 */
public interface SimpleCommand extends Comparable<SimpleCommand> {
	/**
	 * Return the command description.
	 * 
	 * @return The command description.
	 */
	public String getCommandDesc();
	
	/**
	 * Return the command usage.<br>
	 * Example: <code>/myCommand do something</code>
	 * 
	 * @return A description for the user.
	 */
	public String getCommandUsage();
	
	/**
	 * Return the command name.
	 * 
	 * @return The command name.
	 */
	public String getCommandName();
	
	/**
	 * Return the permissions which are need..
	 * 
	 * @return Return the permissions.
	 */
	public Permission getPermission();
	
	/**
	 * Return the command type.
	 * 
	 * @return The command type.
	 */
	public CommandType getCommandType();
	
	/**
	 * Return if the command is only for operators.
	 * (It is needed by Pneumaticrafts CommanHandler).
	 * 
	 * @return False if the command isn't for operators.
	 */
	public boolean isOpRequired();

	/**
	 * {@link AbstractHelpPage} needs it for sorting the commands for the user.
	 */
	public int compareTo(SimpleCommand command);
}
