package de.matt3o12.commands;

import org.bukkit.ChatColor;

/**
 * It is a enum for all commands types. A command type is for a audience of commands for pepole.<br>
 * It is used to show the right colour for a command which is executed by a user, operator, or developer.
 * <br>
 * <table border='1'>
 * 	<tr>
 * 		<th>CommandType</th>
 * 		<th>Colour</th>
 * 	</tr>
 * 
 * 	<tr>
 * 		<td>{@link CommandType#USER_COMMAND}</td>
 * 		<td>{@link ChatColor#AQUA}</td>
 * 	</tr>
 * 
 * 	<tr>
 * 		<td>{@link CommandType#OPERATOR_COMMAND}</td>
 * 		<td>{@link ChatColor#YELLOW}</td>
 * 	</tr>
 * 
 * 	<tr>
 * 		<td>{@link CommandType#DEVELOPER_COMMAND}</td>
 * 		<td>{@link ChatColor#RED}</td>
 * 	</tr>
 * </table>
 * 
 * @author Matt3o12
 */
public enum CommandType {
	/**
	 * The default audience. 
	 */
	USER_COMMAND ( ChatColor.AQUA ),
	
	/** 
	 * The operator audience.
	 */
	OPERATOR_COMMAND ( ChatColor.YELLOW ),
	
	/**
	 * The developer audience.
	 */
	DEVELOPER_COMMAND ( ChatColor.RED );
	
	/** The help colour. */
	private ChatColor chatColor;
	
	/**
	 * Create a CommandType. (It isn't called by you! This is for the compiler!).
	 * 
	 * @param chatColor The chat colour for the audience.
	 */
	CommandType( ChatColor chatColor ){
		this.chatColor = chatColor;
	}
	
	/**
	 * Return the colour which is needed by the help page to display the command in
	 * a pretty colour. (For example: User commands are always display in aqua, 
	 * operator commands in yellow and developer commands in red).
	 *  
	 * @return The chat colour.
	 * @deprecated You should use {@link CommandType#getChatColor()}
	 * @see CommandType#getChatColor()
	 */
	@Deprecated
	public ChatColor getDefuleChatColor(){
		return getChatColor();
	}
	
	/**
	 * Return the colour which is needed by the help page to display the command in
	 * a pretty colour. (For example: User commands are always display in aqua, 
	 * operator commands in yellow and developer commands in red).
	 *  
	 * @return The chat colour.
	 */
	public ChatColor getChatColor() {
		return this.chatColor;
	}
	
	/**
	 * Return sort position for a user command.<br>
	 * <ul>
	 * 	<li>UserCommand: 1</li>
	 * 	<li>OperatorCommand: 2</li>
	 * 	<li>DeveloprtCommand: 3</li>
	 * </ul>
	 * 
	 * It is a reference to {@link CommandType#getPosition()}.
	 * @return Return the sort position.
	 * @see CommandType#getPosition(ChatColor)
	 */
	public int getPosition(){
		return getPosition(getDefuleChatColor());
	}
	
	/**
	 * Return sort position for a chat colour command.<br>
	 * <ul>
	 * 	<li>AQUA: 1</li>
	 * 	<li>YELLOW: 2</li>
	 * 	<li>RED: 3</li>
	 * </ul>
	 * 
	 * @param chatColor The chat colour which is needed.
	 * @return Return the sort position.
	 */
	public static int getPosition( ChatColor chatColor ){
		switch ( chatColor ){
		case RED:
			return 3;
			
		case YELLOW:
			return 2;
			
		case AQUA:
			return 1;
			
		default:
			return 0;
		}
	}
}
