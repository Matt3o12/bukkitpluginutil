package de.matt3o12.commands;

import static de.matt3o12.utils.Language._;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.permissions.Permission;
import org.bukkit.plugin.Plugin;

import de.matt3o12.utils.PermissionInterface;

/**
 * Abstract help page is the default help page and makes easy to create help
 * pages with less code.<br>
 * <h2>How to create a AbstractHelpPage</h2>
 * <p>
 * At first you should create class with ends with "HelpCommand". It is a code
 * guideline!<br>
 * You should add the following commands to the constructor:
 * <ul>
 * <li>All basics command ({@link Matt3o12Command#setName(String)}, ...)
 * <b>without</b> {@link Matt3o12Command#setArgRange(int, int)}.</li>
 * <li>{@link AbstractHelpPage#addCommand(SimpleCommand)} to add all commands
 * which should be included by your help page.</li>
 * </ul>
 * 
 * Example:
 * 
 * <pre>
 * {@code
 * super(plugin);
 * 
 * this.setName("HelpCommand");
 * this.setCommandUsage("/myPlugin help");
 * this.setCommandDesc("A HelpCommand example ;)");
 * this.addKey("myPlugin", 0, 0); // The help page should appear if the user write only ('/myCommand'). 
 * this.addKey("timebutton help", 0, 1); // The help page should appear, too if the user write ('/myPlugin help').
 * 										 // Addition, a page parameter can be added ('/myPlugin help 3').
 * this.addCommandExample("/myPlugin help - Appear the help menu");
 * this.setPermission("", "", PermissionDefault.NOT_OP); // Not recommend to set a custom permission: The help page 
 * 														 // will not show if the user hasn't any permissions for 
 * 														 // following commands.
 * 
 * this.addCommand(new MyCommand1(plugin));				 // Add a command: MyCommand1
 * this.addCommand(new MyCommand2(plugin));				 // Add a command: MyCommand2
 * //...
 * 
 * }</pre>
 * 
 * <b>Notice:</b>
 * The user see only commands which he has permissions.
 * 
 * @author Matteo
 */
public abstract class AbstractHelpPage extends Matt3o12Command{
	/** All commands must be in the list */
	protected ArrayList<SimpleCommand> commands;
	
	/** Max commands per page. Default: 7 */
	protected int maxCommandsPerPage;
	
	/**
	 * Create a new help page.
	 * 
	 * @param plugin Your plugin.
	 */
	public AbstractHelpPage(Plugin plugin) {
		super(plugin);
		
		this.commands = new ArrayList<SimpleCommand>();
		this.setMaxCommandsPerPage(7);
		this.setArgRange(0, 1);
	}
	
	/**
	 * Add a new command to the help menu.
	 * 
	 * @param command The command which should be added.
	 */
	public void addCommand( SimpleCommand command ){
		this.commands.add(command);
	}
	
	/**
	 * Remove a command from the help menu.
	 * 
	 * @param command The command which should be removed.
	 */
	public void removeCommand( SimpleCommand command ){
		this.commands.remove(command);
	}
	
	/**
	 * Clear the help menu; remove all commands.
	 */
	public void removeAllCommands(){
		this.commands.clear();
	}
	
	/**
	 * Returns all commands in a array.<br>
	 * The permissions which the command sender need to execute a command are considered.
	 * (Only the commands will return which the user can execute).
	 * 
	 * @param sender The sender. (Needs to check if he has permissions to show command).
	 * @return A sorted list with all commands. <code>null</code> if no commands found.
	 */
	private SimpleCommand[] getAllCommands( CommandSender sender ){
		TreeSet<SimpleCommand> commandList = new TreeSet<SimpleCommand>();
		PermissionInterface permissions = new PermissionInterface();
		
		for (SimpleCommand command : this.commands){
			Permission comPermissions = command.getPermission();
			
			//don't add command if command is a help page and the help page is empty.
			if (command instanceof AbstractHelpPage){
				AbstractHelpPage helpCommand = (AbstractHelpPage) command;
				
				if (helpCommand.getAllCommands(sender) == null)
					continue;
			}
			
			//Check the permissions. If it failed the command will not added to the return list.
			if ( !permissions.hasPermission( sender, comPermissions.getName(), command.isOpRequired() ) ){
				continue;
			}
			
			commandList.add(command); 
		}
		
		if (commandList.isEmpty())
			return null;
		
		SimpleCommand[] resul = new SimpleCommand[commandList.size()];
				
		int count = 0;
		for (SimpleCommand command : commandList){
			resul[count] = command;
			
			count++;
		}
		
		return resul;
	}
	
	/**
	 * Returns all command for a page.<br>
	 * The permissions which the command sender need to execute a command are considered.
	 * (Only the commands will return which the user can execute).
	 * 
	 * @param sender The sender. (Needs to check if he has permissions to show the command).
	 * @param page The page which is needed.
	 * @return A sorted array with all commands.
	 */
	public SimpleCommand[] getCommands( CommandSender sender, int page ){
		SimpleCommand[] allCommands = this.getAllCommands( sender );
		
		int start = (page - 1) * getMaxCommandsPerPage(); //The start count.
		int maxCount;
		
		maxCount = ( allCommands.length - 1 ) - start;
		maxCount = allCommands.length - start;
				
		if ( maxCount <= 0 ){
			maxCount = ( allCommands.length - 1 );
		} else if (maxCount > getMaxCommandsPerPage()) {
			maxCount = getMaxCommandsPerPage();
		}
		
		SimpleCommand[] commands = new SimpleCommand[maxCount];
		for (int count = 0; count < getMaxCommandsPerPage() && count < maxCount; count++){
			commands[count] = allCommands[count + start];
		}
		
		return commands;
	}
	
	/**
	 * Show the help menu.<br>
	 * It calls only: <code>this.showHelp(sender, 1);</code>
	 * 
	 * @param sender The sender who should see the help menu.
	 * @see AbstractHelpPage#showHelp(CommandSender, int)
	 */
	@Override
	public void showHelp(CommandSender sender){
		this.showHelp(sender, 1);
	}
	
	/**
	 * Return the maximal page count for a sender.
	 * Permissions are considered.
	 * 
	 * @param sender The sender who will see the help menu.
	 * @return The max count for the pages.
	 */
	public int getMaxPages(CommandSender sender) {
		if (getAllCommands(sender) == null)
			return 0;

		if (getAllCommands(sender).length % getMaxCommandsPerPage() == 0) {
			return getAllCommands(sender).length / getMaxCommandsPerPage();
		}

		return getAllCommands(sender).length / getMaxCommandsPerPage() + 1;
	}
	
	/**
	 * Return the help menu.
	 * 
	 * @param sender The sender who should see the help menu.
	 * @param page The page which should be shown.
	 */
	public void showHelp (CommandSender sender, int page){
		if (getAllCommands(sender) == null ){
			String m =  _("noPermissions", "You don't have permissions to do it.");
			sender.sendMessage( ChatColor.RED + m);
			return;
		}
		
		if ( page <= 0 || getMaxPages( sender ) < page ){
			sender.sendMessage(ChatColor.RED + _("abstractHelpCommand.invalidPage", "Please select a valid page."));
			return;
		}
		
		String m1 = _("abstractHelpCommand.pageOf", "Page %s of %s");
		String m2 = _("abstractHelpCommand.possibleCommand", "Possible commands:");
		
		sender.sendMessage(ChatColor.BLUE + "===[ " + this.getCommandName() + " ]===" );
		sender.sendMessage(ChatColor.DARK_AQUA + " " + String.format(m1, page, getMaxPages(sender)));
		sender.sendMessage(ChatColor.DARK_AQUA + " " + m2);

		for (SimpleCommand command : getCommands(sender, page)) {
			sender.sendMessage(command.getCommandType().getChatColor()
					+ command.getCommandUsage() + " - "
					+ command.getCommandDesc());
		}
		
		String m3 = _("abstractHelpCommand.moreInfos", "For more infos, wirte the command without parameters.");
		sender.sendMessage(ChatColor.GRAY + m3);
	}

	/**
	 * Return the help menu.
	 * 
	 * @deprecated
	 * @see AbstractHelpPage#showHelp(CommandSender, int)
	 */
	public void showHelp( int page, CommandSender sender ){
		this.showHelp( sender, page );
	}
	
	/**
	 * Return the max commands per page count.<br>
	 * You shouldn't override it. If you are want to set the command count,
	 * use: {@link AbstractHelpPage#setMaxCommandsPerPage(int)}.
	 * 
	 * @return The max commands per page.
	 */
	public int getMaxCommandsPerPage(){
		return maxCommandsPerPage;
	}
	
	/**
	 * Set the max commands per page.
	 * 
	 * @param maxCommandsPerPage The max commands per page.
	 */
	public void setMaxCommandsPerPage( int maxCommandsPerPage ){
		this.maxCommandsPerPage = maxCommandsPerPage;
	}
	
	@Override
	public void runCommand(CommandSender sender, List<String> args) {
		int defuleMaxCommandsPerPage = getMaxCommandsPerPage();
		
		if ( sender instanceof ConsoleCommandSender ){
			setMaxCommandsPerPage(100); //The console hasn't a confined display and so he can see more as 7 commands.
		}
		
		try{
			int page;
			
			if ( args.size() == 1 ){
				page = new Integer( args.get( 0 ) );
			} else {
				page = 1;
			}
			
			showHelp( sender, page );
		} catch ( NumberFormatException ne ){
			showHelp( sender );
		} finally {
			setMaxCommandsPerPage( defuleMaxCommandsPerPage ); //Reset the count.
		}
	}
}
