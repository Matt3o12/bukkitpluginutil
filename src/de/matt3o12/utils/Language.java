/**
 * 
 */
package de.matt3o12.utils;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.Logger;

/**
 * This class can be use to translate your plugin.<br>
 * When your plugin start it should call {@link Language#setDefaultLanguage(Locale)} to the set the language,
 * which should be use. If you don't do it, the default system language is using but some issues can occur. 
 * (See issues: <a href='https://bitbucket.org/Matt3o12/timebutton/issue/3/wrong-language-on-startup'>
 * https://bitbucket.org/Matt3o12/timebutton/issue/3/wrong-language-on-startup</a>).
 * 
 * @author Matt3o12
 */
public class Language {
	private static final String MESSAGES = "lang.text";
	private static Locale defaultLanguage = Locale.getDefault();
	private static Language instance;
	
	private ResourceBundle defaultBundle;
	
	/**
	 * @see Language#getInstance()
	 */
	private Language(){
		defaultBundle = ResourceBundle.getBundle(MESSAGES, defaultLanguage);
	}
	
	/**
	 * Return the translated value.<br>
	 * A default key should be set therewith the createTranslation.py script create right properties.
	 * (The script isn't public released, yet).
	 * 
	 * @param key The key to identify value.
	 * @param def The default value if the key invalid.
	 * @return The translated value.
	 */
	public String translate(String key, String def){
		try {
			return defaultBundle.getString(key);
		} catch (MissingResourceException e){
			Logger.getLogger("Minecraft").warning("Missing Translation key: " + key);
			return def;
		}
	}

	/**
	 * A short form to: {@link #translate(String, String)}.
	 * 
	 * @see Language#translate(String, String)
	 */
	public static String _(String key, String def){
		return getInstance().translate(key, def);
	}
	
	/**
	 * Return a {@link Language} instance.
	 * @return Return a {@link Language} instance.
	 */
	public static Language getInstance(){
		if (instance == null)
			instance = new Language();
		
		return instance;
	}
	
	/**
	 * Set the default language and ignore the system language.<br>
	 * It is recommend because Bukkit server change the language by herself.
	 * (For more information see issues at project TimeButton:
	 * <a href='https://bitbucket.org/Matt3o12/timebutton/issue/3/wrong-language-on-startup'>
	 * https://bitbucket.org/Matt3o12/timebutton/issue/3/wrong-language-on-startup</a>). 
	 * 
	 * @param langThe language which should be the new default.
	 */
	public static void setDefaultLanguage(Locale lang){
		defaultLanguage = lang;
		instance = null; // It "reload" the bundle for the nex language.
	}
}
