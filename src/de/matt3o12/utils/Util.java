package de.matt3o12.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Util {
	public static CharSequence joinFilePath(Object... paths){
		return join(File.separator, paths);
	}
	
	public static CharSequence join(String sep, Object... list){
		StringBuilder buf = new StringBuilder();
		for (int i = 0; i < list.length; i++){
			String theItem = list[i].toString();
			buf.append(theItem);
			if (i + 1 < list.length)
				buf.append(sep);
		}
		
		return buf;
	}
	
	public static boolean isInt(Object other){
		String o = other.toString();
		try{
			Integer.parseInt(o);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
	
	public static void copyInputStreamToFile(InputStream stream, File file){
		byte[] buf = new byte[1012];
		int len;
		try {
			OutputStream out = new FileOutputStream(file);
			while ((len=stream.read(buf)) > 0){
				out.write(buf, 0, len);
			}
			out.close();
		} catch (IOException e) {
			System.err.println("Unknown Exception:");
			e.printStackTrace();
		}
		
	}
}
