package de.matt3o12.utils;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;

/**
 * A permission handler which makes easy to check if a user has permissions.
 * It is required by Pneumaticrafts CommandHandler.
 * 
 * @author Matt3o12
 */
public class PermissionInterface implements com.pneumaticraft.commandhandler.PermissionsInterface {
	/**
	 * Check if {@link CommandSender} has permissions.<br>
	 * 
	 * @param sender The sender who should have the permissions.
	 * @param node A permissions node. (Zb: CastleWars.createArea)
	 * @param isOpRequired Represents the default value for the permissions.
	 * 
	 * @return True, if {@link CommandSender} has permissions.
	 */
	@Override
	public boolean hasPermission(CommandSender sender, String node,
			boolean isOpRequired) {
		Permission perm = new Permission(node);
		if (isOpRequired)
			perm.setDefault(PermissionDefault.OP);
		else
			perm.setDefault(PermissionDefault.NOT_OP);
		
		return sender.hasPermission(perm);
	}
	
	/**
	 * Returns if the sender has any permission.<br>
	 * I know, opRequired is strange because every node should have a own opRequired value but 
	 * {@link com.pneumaticraft.commandhandler.PermissionsInterface} by Pneumaticraft requires it.
	 * 
	 * @param sender The sender who should have the permission.
	 * @param nodes All nodes.
	 * @param opRequired Represnets the default value for the permissions.
	 * @return If sender has any permissions of nodes.
	 */
	@Override
	public boolean hasAnyPermission(CommandSender sender,
			List<String> nodes, boolean opRequired) {
		
		for (String node : nodes) {
			if (hasPermission(sender, node, opRequired))
				return true;

		}
		
		return false;
	}

	/**
	 * Returns if the sender has all of the permission.<br>
	 * I know, opRequired is strange because every node should have a own opRequired value but 
	 * {@link com.pneumaticraft.commandhandler.PermissionsInterface} by Pneumaticraft requires it.
	 * 
	 * @param sender The sender who should have the permission.
	 * @param nodes All nodes.
	 * @param opRequired Represnets the default value for the permissions.
	 * @return If sender has all permissions of nodes.
	 */

	@Override
	public boolean hasAllPermission(CommandSender sender,
			List<String> nodes, boolean opRequired) {
		

		for ( String node : nodes ){
			if ( !hasPermission( sender, node, opRequired ) )
				return false;
			
		}
		
		return true;
	}
	
	@Deprecated
	/**
	 * Unused.
	 * @return Return my OfflinePlayer Matt3o12.
	 */
	public OfflinePlayer[] getDevelopers(){
		OfflinePlayer[] offlinePlayers = { 
				Bukkit.getOfflinePlayer( "Matt3o12" ),
				};	
		
		return offlinePlayers;
	}
}
